package seiot.dumpster_service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/*
 * Data Service as a vertx event-loop 
 */
public class DumpsterService extends AbstractVerticle {

	private int port;
	private static final int Ndays = 7;
	// private LinkedList<DataDumpster> values;
	private DataDumpster dumpster;
	private Date date;
	private DateFormat df;
	
	public DumpsterService(int port) {
		//values = new LinkedList<>();		
		this.dumpster = new DataDumpsterImpl();
		this.port = port;
		this.date = null;
		df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ITALY);
	}

	@Override
	public void start() {		
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		//router.post("/api/data").handler(this::handleAddNewData);
		router.get("/api/status").handler(this::handleGetStatus);
		router.get("/api/andamento").handler(this::handleGetDumpsterUtilization);
		router.get("/api/cambiastatus").handler(this::handleChangeStatus);
		router.post("/api/available").handler(this::handleSetAvaiable);
		router.post("/api/notavailable").handler(this::handleSetNotAvaiable);
		router.post("/api/peso").handler(this::handleSetQuantitaCorrente);
		router.get("/api/token").handler(this::handleGetLogin);
		router.post("/api/deposito").handler(this::handlePostDeposito);
		vertx
			.createHttpServer()
			.requestHandler(router::accept)
			.listen(port);

		log("Service ready.");
	}
	
	/*** Stato corrente Dumpster 
	 *   Sia Dashboard che Esp.
	 *   Serve all'esp per monitorare quantita corrente nel dumpster ed eventualmente farlo passare
	 *   in Not-Avaiable quando si riempie***/
	private void handleGetStatus(RoutingContext routingContext) {//DASHBOARD - stato
		JsonObject dati = new JsonObject();	
		received("dashboard chiede stato");
		Calendar calendar = Calendar.getInstance();
		if(date == null) {
			date = new Date();
		} 
		calendar.setTime(date);
		Date al = calendar.getTime();
		String dateString = df.format(al);

		dati.put("stato", "Not Available");
		if(this.dumpster.isAvaiable()) {
			dati.put("stato", "Available");
		}
		dati.put("depositi", this.dumpster.getNumDepositi(dateString));
		dati.put("quantitaCorrente", this.dumpster.getQuantitaCorrente());
		
		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(dati.encodePrettily());
	}
	
	/*** statistiche utilizzo Dumpster negli ultimi N giorni.
	 *   Dashboard
	 *   ***/
	private void handleGetDumpsterUtilization(RoutingContext routingContext) {
		JsonObject dati = new JsonObject();	
		Calendar calendar = Calendar.getInstance();
		Integer numDepositi = 0;
		Integer counter = 0;
		
		received("Dashboard - utilizzo");
		
		if(this.date == null) {
			this.date = new Date();
		} 
		calendar.setTime(date);
		Date al = calendar.getTime();
		String updateData = df.format(al);
		dati.put("al", df.format(calendar.getTime()));
		
		while(counter!=Ndays) { //calcolo delle quantità e depositi negli N-days
			int numDepositiInData = this.dumpster.getNumDepositi(updateData);
			numDepositi += numDepositiInData;
			calendar.add(Calendar.DATE, -1);
			updateData = df.format(calendar.getTime());
			calendar.setTime(calendar.getTime());
			counter++;
		};
		dati.put("dal", updateData);
		dati.put("depositi", numDepositi);

		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(dati.encodePrettily());
	}
	
	/***Dashboard forza il cambiamento di stato **/
	private void handleChangeStatus(RoutingContext routingContext) {  
		JsonObject dati = new JsonObject();	
		received("Change Status");
		
		String stato = "Disponibile";
		if(this.dumpster.isAvaiable()) {
			stato = "NonDisponibile";
		}
		dati.put("cambiaStato", stato);
		this.dumpster.changeStatus();
		
		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(dati.encodePrettily());
	}
	
	/*** Esp setta l'avaiability del Dumpster ***/
	private void handleSetAvaiable(RoutingContext routingContext) {
		received("Set avaiability");
		this.dumpster.setAvailability(true);
	}
	
	/*** Esp setta la non avaiability del Dumpster ***/
	private void handleSetNotAvaiable(RoutingContext routingContext) {
		received("Set not avaiability");
		this.dumpster.setAvailability(false);
		
		routingContext.response()
		.putHeader("content-type", "application/json")
		.end();
	}
	
	/***Esp setta quantità corrente nel Dumpster ***/
	private void handleSetQuantitaCorrente(RoutingContext routingContext){
		JsonObject res = routingContext.getBodyAsJson();
		received("Set quantità corrente nel dumpster");
		this.dumpster.setQuantitaCorrente(res.getInteger("value"));
		
		routingContext.response()
		.putHeader("content-type", "application/json")
		.end();
	}
	
	/*** Richiesta login da Mobile ***/
	private void handleGetLogin(RoutingContext routingContext) {
		JsonObject dati = new JsonObject();	
		received("Login");
		String token = "Ok";
		if(!this.dumpster.isAvaiable()) {
			token = "No";
		}
		dati.put("token", token);
		
		routingContext.response()
			.putHeader("content-type", "application/json")
			.end(dati.encodePrettily());
	}
	
	/** Deposito **/
	private void handlePostDeposito(RoutingContext routingContext) {

		received("Deposito");
		Calendar calendar = Calendar.getInstance();
		if(this.date == null) {
			this.date = new Date();
		} 
		calendar.setTime(date);
		Date date = calendar.getTime();
		String stringDate = df.format(date);
		
		this.dumpster.newDeposito(stringDate);
		
		routingContext.response()
		.putHeader("content-type", "application/json")
		.end();
	}
	
		
	
	/**
	 * metodo per stampare cosa si riceve
	 * @param s stringa ricevuta
	 */
	private void received(String s) {
		log("Received: "+ s);
	}

	private void log(String msg) {
		System.out.println("[DATA SERVICE] "+msg);
	}

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		DumpsterService service = new DumpsterService(8080);
		vertx.deployVerticle(service);
	}
}