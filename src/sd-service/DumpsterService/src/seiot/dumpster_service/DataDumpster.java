package seiot.dumpster_service;

public interface DataDumpster {
	
	/***It checks whether the dumpster is available  ***/
	public boolean isAvaiable();
	
	/***It changes the state of the dumpster. 
	 * If it was available it becomes unavailable and vice versa***/
	public boolean changeStatus();
	
	/**It sets whether the dumpster is available or not **/
	public void setAvailability(boolean status);
	
	/**To get the number of deposits made in a day **/
	public int getNumDepositi(String date);
	
	/** To obtain the amount of waste present in the dumpster **/
	public int getQuantitaCorrente();
	
	/** To reset the amount of waste present in the dumpster **/
	public void reset();
	
	/**It add new deposit made in one day **/
	public void newDeposito(String date);
	
	/** To set the amount of waste present in the dumpster **/
	public void setQuantitaCorrente(int quantita);

}
