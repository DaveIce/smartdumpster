package seiot.dumpster_service;


import java.util.HashMap;
import java.util.Map;

/**Classe che tiene i dati del dumpster in un determinato giorno **/
class DataDumpsterImpl implements DataDumpster{
	
	private boolean avaiable;
	private int quantitaCorrente;
	private Map<String, Integer> depositi; 

		
	public DataDumpsterImpl() {
		this.depositi = new HashMap<>();
		this.quantitaCorrente = 0;
		this.avaiable = true;
	}
	
	public boolean isAvaiable() {
		return this.avaiable;
	}
	
	public boolean changeStatus() {
		if(this.isAvaiable()) {
			this.avaiable = false;
			return this.avaiable;
		}
		this.avaiable = true;
		return this.avaiable;
	}
	
	public void setAvailability(boolean status) {
		this.avaiable = status;		
	}
	
	
	public int getNumDepositi(String date) {
		return this.depositi.getOrDefault(date, 0);
	}
	
	public int getQuantitaCorrente() {
		return this.quantitaCorrente;
	}
	
	public void reset() {
		this.quantitaCorrente = 0;
	}
	
	public void newDeposito(String date) {
		if(!this.depositi.containsKey(date)) {
			this.depositi.put(date, 0);
		}
		int numDepositi = this.depositi.get(date);
		numDepositi += 1;
		depositi.put(date, numDepositi);
	}
	
	//metodo per l'Esp
	public void setQuantitaCorrente(int quantita) {
		this.quantitaCorrente = quantita;
	}
	
}
