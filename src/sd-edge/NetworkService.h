#ifndef __NETWORKSERVICE__
#define __NETWORKSERVICE__

#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <string>

class NetworkService { 
public:
  NetworkService(String ssid, String pwd, String address);
  void sendData(String address, int value, String place);
  String getState(String address, String place);    
private:
  HTTPClient availability;
  HTTPClient weight;
  HTTPClient full;
  bool checkConnection();
};

#endif
