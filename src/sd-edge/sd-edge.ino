#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include "NetworkService.h"
#include "Led.h"
#include "PotentiometerImpl.h"

#define LEDA 4
#define LEDNA 5
#define POT A0

#define MAX_WEIGHT 900

/* wifi network name */
char* ssidName = "TIM-28010363";
/* WPA2 PSK password */
char* pwd = "jHsJJjm016Ihf2xlj6R2Z7XN";
/* service IP address */
char* address = "http://cf20b728137d.ngrok.io";

NetworkService* network;
Led* ledAvailable;
Led* ledNAvailable;
Potentiometer* pot;

void setup() {
  Serial.begin(115200);
  network = new NetworkService(ssidName, pwd, address);
  ledAvailable = new Led(LEDA);
  ledNAvailable = new Led(LEDNA);
  pot = new PotentiometerImpl(POT);
  
}

void loop() {
  network->sendData(address, pot->getValue(), "peso");
  
  if(pot->getValue() > MAX_WEIGHT){
    network->sendData(address, 0, "notavailable");
    ledNAvailable->switchOn();
    ledAvailable->switchOff();
  }
  
  String state = network->getState(address, "home");
  if(state == "A") {
      ledAvailable->switchOn();
      ledNAvailable->switchOff();
  } else if (state == "N") {
      ledNAvailable->switchOn();
      ledAvailable->switchOff();
  } else {
    Serial.println("Error in String state");
  }
  delay(500);
}
