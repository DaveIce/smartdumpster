#include "NetworkService.h"


NetworkService::NetworkService(String ssid, String pwd, String address){
  WiFi.begin(ssid, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected: \n local IP: "+ WiFi.localIP());
  this->availability.begin(address + "/api/status");
  this->weight.begin(address + "/api/peso");
  this->weight.addHeader("Content-Type", "application/json");
  this->full.begin(address + "/api/notavailable");
}

void NetworkService::sendData(String address, int value, String call){
  String msg = "";
  if(this->checkConnection()){

    
    if(call == "peso"){
      msg = String("{ \"value\": ") + String(value) + " }";
      this->weight.POST(msg);
    }else{
      this->full.POST(msg);
    }
    
    
    // String payload = http.getString();
    // Serial.println(payload);
  }
}

String NetworkService::getState(String address, String place){
  String string = "No"; //DEFAULT NOT AVAILABLE
  if(this->checkConnection()){
    int result = this->availability.GET();
    if(result > 0){
      string = this->availability.getString().substring(16,17);
      Serial.println(string);
    }
  }
  return string; 
}

bool NetworkService::checkConnection(){
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Error!");
    return false;
  }
  return true;
};
