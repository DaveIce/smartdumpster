package com.example.smartdumpsterapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

import btlib.BluetoothChannel;
import btlib.BluetoothUtils;
import btlib.ConnectToBluetoothServerTask;
import btlib.ConnectionTask;
import btlib.RealBluetoothChannel;
import btlib.exceptions.BluetoothDeviceNotFound;

public class MainActivity extends AppCompatActivity {
    private static final String SERVICE_ADDRESS = "http://cf20b728137d.ngrok.io";
    private BluetoothChannel btChannel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        if(btAdapter != null && !btAdapter.isEnabled()){
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }
        initUI();
    }

    private void initUI() {
        findViewById(R.id.connectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    connectToBTServer();
                } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
                    bluetoothDeviceNotFound.printStackTrace();
                }
            }
        });

        findViewById(R.id.connectionStatusBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ConnectivityManager cm = (ConnectivityManager) MainActivity.this.getSystemService(Context.CONNECTIVITY_SERVICE);

                final NetworkInfo activeNetwork = Objects.requireNonNull(cm).getActiveNetworkInfo();

                if (activeNetwork.isConnectedOrConnecting()) {
                    ((TextView) MainActivity.this.findViewById(R.id.resLabel)).setText("Network is connected");
                }
            }
        });
/*
        findViewById(R.id.getBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.tryHttpGet();
            }
        });

        findViewById(R.id.postBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MainActivity.this.tryHttpPost();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        */

    }

    private void tryHttpGet(final String type){
        //final String url = "https://jsonplaceholder.typicode.com/todos/1";
        final String url = SERVICE_ADDRESS + "/api/token";
        Http.get(url, new Http.Listener() {
            @Override
            public void onHttpResponseAvailable(HttpResponse response) {
                if (response.code() == HttpURLConnection.HTTP_OK) {
                    try {
                        String token = response.contentAsString().substring(14,16);
                        ((TextView) MainActivity.this.findViewById(R.id.resLabel)).setText(token);
                        if(token.equals("Ok")){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    btChannel.sendMessage(type);
                                }
                            }).start();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void depositOver(final String type) throws JSONException {
        final String url = SERVICE_ADDRESS + "/api/deposito";
        final String content = new JSONObject()
                .put("type",type).toString();

        Http.post(url, content.getBytes(), new Http.Listener() {
            @Override
            public void onHttpResponseAvailable(HttpResponse response) {
                String postSuccess = "Deposit completed successfully!\nYou can now select a type of waste to start another deposit:";
                String postFailed = "Couldn't notify server about deposit, check connection!";
                if(response == null){
                    ((TextView) MainActivity.this.findViewById(R.id.statusBT)).setText(postFailed);
                }else{
                    ((TextView) MainActivity.this.findViewById(R.id.statusBT)).setText(postSuccess);
                }
            }
        });
    }

    /*BLUETOOTH*/

    @Override
    protected void onStop() {
        super.onStop();

        btChannel.close();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK) {
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if (requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED) {
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }


    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);

        final UUID uuid = BluetoothUtils.getEmbeddedDeviceDefaultUuid();
        //final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {

                ((TextView) findViewById(R.id.statusBT)).setText(String.format("Successfully connected to %s, the dumpster bluetooth server!\nYou can now select a type of waste to start a deposit:",
                        serverDevice.getName()));

                findViewById(R.id.connectBtn).setEnabled(false);

                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        if(receivedMessage.charAt(0) == 'F'){
                            try {
                                MainActivity.this.depositOver("A");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            findViewById(R.id.sendA).setEnabled(true);
                            findViewById(R.id.sendB).setEnabled(true);
                            findViewById(R.id.sendC).setEnabled(true);
                        }
                        /*
                        ((TextView) findViewById(R.id.chatLabel)).append(String.format("> [RECEIVED from %s] %s\n",
                                btChannel.getRemoteDeviceName(),
                                receivedMessage));
                         */
                    }

                    @Override
                    public void onMessageSent(String sentMessage) {
                        if(sentMessage.equals("T")){
                            ((TextView)findViewById(R.id.statusBT)).append("\nYou have been successfully granted additional time to complete the deposit!");
                        }else{
                            ((TextView)findViewById(R.id.statusBT)).setText("Type of waste selected: " + sentMessage + "\nDeposit in progress...");
                            findViewById(R.id.sendA).setEnabled(false);
                            findViewById(R.id.sendB).setEnabled(false);
                            findViewById(R.id.sendC).setEnabled(false);
                        }

                        /*
                        ((TextView) findViewById(R.id.chatLabel)).append(String.format("> [SENT to %s] %s\n",
                                btChannel.getRemoteDeviceName(),
                                sentMessage));
                         */
                    }
                });

                findViewById(R.id.sendA).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.this.tryHttpGet("A");
                    }
                });
                findViewById(R.id.sendB).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.this.tryHttpGet("B");
                    }
                });
                findViewById(R.id.sendC).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.this.tryHttpGet("C");
                    }
                });
                findViewById(R.id.sendT).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                btChannel.sendMessage("T");
                            }
                        }).start();
                    }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.resLabel)).setText(String.format("Status : unable to connect, device %s not found!",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }

}