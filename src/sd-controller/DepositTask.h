#ifndef __DEPOSITTASK__
#define __DEPOSITTASK__

#include "Dumpster.h"

class DepositTask {

  int TimeDeliver;
  Dumpster* dumpster;

public:
  DepositTask(Dumpster* smartDumpster);
  void tick();

private:
  void checkMoreTime();
  String readBT();
  void turnLedOn(String message);
  void turnLedOff();
};


#endif
