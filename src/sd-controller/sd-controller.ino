#include "DepositTask.h"

DepositTask* mainTask;
Dumpster* smartDumpster;

void setup() {
  Serial.begin(9600);
  smartDumpster = new Dumpster();
  mainTask = new DepositTask(smartDumpster);
  smartDumpster->getBTService()->init();
  while (!Serial){}
  Serial.println("ready to go."); 
}

void loop() {
   mainTask->tick();
}
