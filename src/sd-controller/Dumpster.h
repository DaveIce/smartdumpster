#ifndef __DUMPSTER__
#define __DUMPSTER__

#include "Led.h"
#include "MsgServiceBT.h"
#include "ServoMotorImpl.h"

class Dumpster {
public:
  Dumpster();
  void openGate();
  void closeGate();
  int getPendingDeposit();
  void requestDeposit();
  void depositDone();
  int isGateOpen();  // 1 if the gate is open, else 0
  void setGatePosition();
  MsgServiceBT* getBTService();
  void setMessage(String mes);
  String getMessage();
  Led* typeA;
  Led* typeB;
  Led* typeC;

private:
  String message;
  MsgServiceBT* BTService;
  int gatePosition; // 1 if the gate is open, else 0
  int pendingDeposit;
  ServoMotorImpl* servo;
};

#endif
