#include "Arduino.h"
#include "Dumpster.h"

/* Define components' pin. */
#define LED_A_PIN 8
#define LED_B_PIN 9
#define LED_C_PIN 10
#define SERVO_PIN 6
#define BT_PIN_READ 2
#define BT_PIN_WRITE 3

/* Constructor */
Dumpster::Dumpster() {
  this->typeA = new Led(LED_A_PIN);
  this->typeB = new Led(LED_B_PIN);
  this->typeC = new Led(LED_C_PIN);
  this->servo = new ServoMotorImpl(SERVO_PIN);
  this->pendingDeposit = 0;
  this->gatePosition = 0;
  this->closeGate();
  this->BTService = new MsgServiceBT(2, 3);
}


/* -----PUBLIC METHODS----- */


int Dumpster::getPendingDeposit() {
  return this->pendingDeposit;
}

void Dumpster::requestDeposit() {
  this->pendingDeposit++;
}

void Dumpster::depositDone() {
  this->pendingDeposit--;
}

int Dumpster::isGateOpen() {
  return this->gatePosition;
}

MsgServiceBT* Dumpster::getBTService() {
  return this->BTService;
}

void Dumpster::setMessage(String mes) {
  this->message = mes;
}

String Dumpster::getMessage() {
  return this->message;
}

/* -----PRIVATE METHODS----- */
void Dumpster::openGate() {
  this->servo->on();
  this->servo->setPosition(180);
}

void Dumpster::closeGate() {
  this->servo->on();
  this->servo->setPosition(0);
}
