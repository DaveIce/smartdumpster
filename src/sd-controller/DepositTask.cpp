#include "DepositTask.h"

DepositTask::DepositTask(Dumpster* smartDumpster) {
  this->dumpster = smartDumpster;
  this->TimeDeliver = 5000;
}

void DepositTask::tick(){
  String command = this->readBT();
  Serial.println(command);
  if(command != "") {
    this->turnLedOn(command);
    dumpster->openGate();
    delay(this->TimeDeliver);
    this->checkMoreTime();
    dumpster->closeGate();
    delay(500);
    this->turnLedOff();
    this->dumpster->getBTService()->sendMsg(Msg("F"));
  }
}

void DepositTask::checkMoreTime() {
  String moreTime = this->readBT();
  if(moreTime == "T") {
    delay(10000);
    moreTime = "";
    this->checkMoreTime();
  }
}

String DepositTask::readBT() {
  MsgServiceBT* btService = this->dumpster->getBTService();
  if(btService->isMsgAvailable()) {
      String mes = btService->receiveMsg()->getContent();
      return mes;
  }
  return "";
}

void DepositTask::turnLedOn(String message) {
    if(message == "A"){
      dumpster->typeA->switchOn();
    }
    if(message == "B"){
      dumpster->typeB->switchOn();
    }
    if(message == "C"){
      dumpster->typeC->switchOn();
    }
}

void DepositTask::turnLedOff() {
  dumpster->typeA->switchOff();
  dumpster->typeB->switchOff();
  dumpster->typeC->switchOff();
}
