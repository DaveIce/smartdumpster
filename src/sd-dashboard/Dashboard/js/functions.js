function stampaStato(stato){
    console.log(stato["disponibilità"]);
    let result = `
        <br>
        <article>
            <span class="text-center  py3">
               ${stato["stato"]}
            </span> <br>
            <span class="text-center py3">
               Numero Depositi effettuati oggi: ${stato["depositi"]}
            </span> <br>
            <span class="text-center py3">
               Quantità corrente nel dumpster: ${stato["quantitaCorrente"]}
            </span>
        </article>
        `;
    return result;
}



function stampaCambiaStato(cambiaStato){
    let result = `
    <br>
    <br>
    <article>
        <span class="text-center py3">
           Stato del dumpster cambiato ora è: ${cambiaStato["cambiaStato"]}
        </span>
    </article>
    `;

    return result;
}

function stringaToID(stringa){
    return stringa.toLowerCase().replace(/[^a-zA-Z]/g, "");
}

function stampaAndamento(andamento){

    let result = `
        <br>
        <br>
        <article>
            <header>
                <h2>dal ${andamento["dal"]} al ${andamento["al"]}  </h2>
            </header>
            <section>
                <p>Depositi: ${andamento["depositi"]}</p>
            </section>
        </article>
    `;

    return result;
}

/*
$.post("/api/whiskies", JSON.stringify({name: name, origin: origin}), function () {
    load();
}, "json");
*/
/*
$.post(url, JSON.stringify({request: "stato"}), function (data) {
    content = stampaStato(data);
    $("main").html(content);
}, "json");
*/

function updateContent(page){
    let content;
    /*cambiare di volta in volta la parte esadecimale dell'indirizzo */
    let url = "http://cf20b728137d.ngrok.io";
    switch(page){
        case "Stato":
            $.getJSON(url + "/api/status", function (data) {
                //console.log(data["disponibilita"])
                content = stampaStato(data);
                console.log(content);
                $("main").html(content);

            });
            break;
        case "CambiaStato":
            $.getJSON(url + "/api/cambiastatus", function(data){
                content = stampaCambiaStato(data);
                $("main").html(content);
            });
            break;
        case "Andamento":
            $.getJSON(url + "/api/andamento", function(data){
                content = stampaAndamento(data);
                console.log(content);
                $("main").html(content);
            });
            break;

    }

}
