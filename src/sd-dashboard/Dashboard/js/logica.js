$(document).ready(function(){
   let currentPage = "Stato";
   updateContent(currentPage);
   $(".home").addClass("active");
   
   $(".nav-link").click(function(){
        let clickedPage = $(this).text();
        if(clickedPage !== currentPage){
            $(".nav-link").removeClass("active");
            $(this).addClass("active");
            currentPage = clickedPage;
        }
        updateContent(currentPage);
   });
});

